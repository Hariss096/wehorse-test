const fs = require("fs");
var Router = require('koa-router');

//Prefix all routes with /courses
var coursesRouter = Router({
   prefix: '/courses'
});

fs.access("mock_db.json", fs.F_OK, (error) => {
   if (error) throw error;

   fs.unlinkSync("mock_db.json", (error) => {
      if (error) throw error;
      });
   });

const courses = [{"courseId":0,"courseName":"Mock course created","wishListFlag":true}];

// CREATE
const createCourse = async (ctx, next) => {
   //Check if course name is provided
   if(!ctx.request.body.courseName){
      ctx.response.status = 400;
      ctx.body = {message: "400 Bad Request"};
   } else {
      var newId = courses[courses.length-1].courseId+1;
      courses.push({
         courseId: newId,
         courseName: ctx.request.body.courseName,
         wishListFlag: ctx.request.body.wishListFlag
      });

      fs.writeFile("mock_db.json", JSON.stringify(courses), (error) => {
         if (error) throw error;
         console.log("Mock Database updated");
      });
      ctx.body = {message: "New course created", location: "/courses/" + newId};
   }
};


// READ All
const getAllCourses = (ctx, next) => {
   if ("wishListFlag" in ctx.request.query) {
      // Filter based on wishListFlag
      ctx.body = courses.filter((course) => String(course.wishListFlag) == ctx.request.query.wishListFlag);
   }
   else {
      ctx.body = courses;
   }
};

// READ WITH ID
function getCourseById(ctx, next) {
   const currentCourse = courses.filter( (course) => {
      if(course.courseId == ctx.params.id){
         return true;
      }
   });
   if(currentCourse.length == 1){
      ctx.body = currentCourse[0];
   } else {
      ctx.response.status = 404;
      ctx.body = {message: "404 Not Found"};
   }
};

// UPDATE
const updateCourse = (ctx, next) => {
   //Check if course name is provided
   if(!ctx.request.body.courseName){
      ctx.response.status = 400;
      ctx.body = {message: "400 Bad Request"};
   } else {
      //Get id of course to update
      var updateIndex = courses.map( (course) => {
         return course.courseId;
      }).indexOf(parseInt(ctx.params.id));
      
      if(updateIndex === -1){
         //Course not found, create new
         courses.push({
            courseId: ctx.params.id,
            courseName: ctx.request.body.courseName,
            wishListFlag: ctx.request.body.wishListFlag
         });
         fs.writeFile("mock_db.json", JSON.stringify(courses), (error) => {
            if (error) throw error;
            console.log("Mock Database updated");
         });
         ctx.body = {message: "New Course created", location: "/courses/" + ctx.params.id};
      } else {
         //Update existing course
            courses[updateIndex] = {
               courseId: ctx.params.id,
               courseName: ctx.request.body.courseName,
               wishListFlag: ctx.request.body.wishListFlag
         };
         fs.writeFile("mock_db.json", JSON.stringify(courses), (error) => {
            if (error) throw error;
            console.log("Mock Database updated");
         });
         ctx.body = {message: "Course id " + ctx.params.id + " updated", 
            location: "/courses/" + ctx.params.id};
      }
   }
};

// DELETE
const deleteCourse = (ctx, next) => {
   var courseToDeleteId = courses.map( (course) => {
      return course.courseId;
   }).indexOf(ctx.params.id);
   
   if(courseToDeleteId === -1){
      ctx.body = {message: "Not found"};
   } else {
      courses.splice(courseToDeleteId, 1);
      fs.writeFile("mock_db.json", JSON.stringify(courses), (error) => {
         if (error) throw error;
         console.log("Mock Database updated");
      });
      ctx.body = {message: "Course id " + ctx.params.id + " removed"};
   }
};

coursesRouter.post('/', createCourse);
coursesRouter.get("/", getAllCourses);
coursesRouter.get('/:id', getCourseById);
coursesRouter.put('/:id', updateCourse);
coursesRouter.delete('/:id', deleteCourse);


module.exports.coursesRouter = coursesRouter;