Simple API Service for creating, reading, updating and deleting courses from mock_db (json file)

To run server:
    - install dependencies                  : npm install
    - start server                          : node index.js

The use of this API services is provided below.

- API Endpoints:

    - $BASE_URL                             : http://localhost:5000/ (default)
    - Get all courses                       : GET / $BASE_URL/courses
    - Get a single course by ID             : GET / $BASE_URL/courses/:id
    - Add new course                        : POST / $BASE_URL/courses
    - Update existing course by ID          : PUT / $BASE_URL/courses/:id
    - Delete course by ID                   : DELETE / $BASE_URL/courses/:id

- Query params:
    - Filter based on wishListFlag          : $BASE_URL/courses?wishListFlag=value

- Packages used:
    - Koa (An extension of Express.js web framework for Node)