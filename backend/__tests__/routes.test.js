const request = require('supertest');
const server = require("../index");
const fs = require("fs");

// Mock Data
const courses = [{"courseId":0,"courseName":"Mock course created","wishListFlag":true}];
let mockCourse = {
    "courseName": "Mock course created",
    "wishListFlag": true
};

beforeAll(async () => {
 console.log('Jest starting !');
    fs.access("../mock_db.json", fs.F_OK, (error) => {
        // if (error) throw error;
    
        fs.unlinkSync("../mock_db.json", (error) => {
        if (error) throw error;
        });
    });
    fs.writeFile("../mock_db.json", JSON.stringify(courses), (error) => {
        if (error) throw error;
     });
});

// close the server after testing
afterAll(() => {
 server.close();
 console.log('server closed !');
});

// Testing endpoints
it('successfully calls GET / endpoint', async done => {
    const response = await request(server).get('/courses')
    expect(response.status).toBe(200);
    done();
  });

it('successfully calls POST / endpoint', async done => {
    const response = await request(server).post('/courses/0')
    expect(response.status).toBe(404);
    done();
  });

  it('successfully calls PUT / endpoint', async done => {
    const response = await request(server).put('/courses/0')
    expect(response.status).toBe(400);
    done();
  });

  it('successfully calls DELETE / endpoint', async done => {
    const response = await request(server).delete('/courses/0')
    expect(response.status).toBe(200);
    done();
  });

// Testing endpoints with payloads
describe('test create course in db', () => {
    test('courses POST /', async () => {
        const response = await request(server).post('/courses').send(mockCourse);
        expect(response.status).toEqual(200);
        expect(JSON.parse(response.text)).toEqual({message: "New course created", location: "/courses/1"});
    });
});

describe('test home route', () => {
    test('root route GET /', async () => {
        const response = await request(server).get('/');
        expect(response.status).toEqual(200);
        expect(response.text).toContain('Hello World!');
    });
});

describe('testing courses route', () => {
    test('courses route GET /', async () => {
        const response = await request(server).get('/courses');
        expect(response.status).toEqual(200);
        console.log("type bhai     !!!  ", typeof(response.text))
        expected_value = [{
            "courseId":0,
            "courseName":"Mock course created",
            "wishListFlag":true},
            {"courseId":1,
            "courseName":"Mock course created",
            "wishListFlag":true}];
        expect(JSON.parse(response.text)).toEqual(expected_value);
    });
});

describe('test update course with New ID 1', () => {
    test('courses PUT /', async () => {
        const response = await request(server).put('/courses/1').send({courseName: "Course 1 updated", wishListFlag: true});
        expect(response.status).toEqual(200);
        expect(JSON.parse(response.text)).toEqual({message: "Course id 1 updated", location: "/courses/1"});
    });
});


describe('test delete course from db', () => {
    test('courses DELETE /', async () => {
        const response = await request(server).delete('/courses/1');
        expect(response.status).toEqual(200);
        expect(JSON.parse(response.text)).toEqual({message: "Course id 1 removed"});
    });
});