// Web framework
var koa = require('koa');
const cors = require('@koa/cors');
const Router = require('koa-router');

const router = new Router();
const { coursesRouter } = require('./courses.js');

// Middlewares
var logger = require('koa-logger');
var bodyParser = require('koa-body');



var app = new koa();

app.use(cors()); // handle CORS errors

app.use(logger());

app.use(async (ctx, next) => {
    // global exception handling
    try {
        await next();
    } catch (err) {
        ctx.status = err.status || 500;
        ctx.body = err.message;
        ctx.app.emit('error', err, ctx);
    }
   });

//Set up body parsing middleware
app.use(bodyParser({
   formidable:{uploadDir: './uploads'},
   multipart: true,
   urlencoded: true
}));

// Test route
router.get('/', (ctx, next) => {
    ctx.body = 'Hello World!';
   });

app.use(router.routes());
app.use(coursesRouter.routes());



const server = app.listen(5000);
module.exports = server;