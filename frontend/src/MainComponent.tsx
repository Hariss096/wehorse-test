import React from 'react';
import './MainComponent.css';
import { connect } from "react-redux"

import * as actions from "./redux/actions";
import { CourseComponent } from "./components/CourseComponent"


type Props = {
  courseList: [],
  editCourseInfo: (id: number, payload: any) => void
  updateWishList: (course: object | any) => void
}

const MainComponent: React.FC<Props> = (props) => {
  const courseList = props.courseList;

  const handleCourseUpdate = (course: Course) => {
    props.updateWishList(course);
    props.editCourseInfo(course.courseId, course)
  }

  return (
    <main>
      <h1>Courses List</h1>
      {courseList.map((course: Course) => (
        <CourseComponent
          key={course.courseId}
          course={course}
          onCourseUpdate={handleCourseUpdate}
        />
      ))}
    </main>
  )
}

const mapStateToProps = (state: any) => {
	const { CReducer } = state;
	return {
		courseList: CReducer.courseList
	}
	
  }

  const mapDispatchToProps = ({
    updateWishList: actions.updateWishList,
    editCourseInfo: actions.editCourseInfo
  })


export default connect(mapStateToProps, mapDispatchToProps)(MainComponent);