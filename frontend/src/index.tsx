import './index.css';
import React from 'react';
import thunk from "redux-thunk"
import ReactDOM from 'react-dom';
import logger from "redux-logger";
import { Provider } from "react-redux";
import rootReducer from "./redux/reducers";
import MainComponent from './MainComponent';
import { createStore, applyMiddleware } from "redux";
import { BrowserRouter as Router, Route } from 'react-router-dom';

const middlewares = [logger, thunk];
const store = createStore(rootReducer, applyMiddleware(...middlewares));

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route path="/" exact component={MainComponent} />
      <Route path="/courses" exact component={MainComponent} />
    </Router>
  </Provider>, document.getElementById("root")
)
