import React from 'react';
import { render } from '@testing-library/react';
import { unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { CourseComponent } from './CourseComponent';

// Mocks
let container: any = null;
var props = {
    course : {
    "courseId": 0,
    "courseName": "Mock course created",
    "wishListFlag": true
    },
    onCourseUpdate: (course: Course) => {}
  };

beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
  });
  
  afterEach(() => {
    // cleanup on exit
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

// Tests
it("renders with given props", () => {
    act(() => {
      render(<CourseComponent
        key={props.course.courseId}
        course={props.course}
        onCourseUpdate={props.onCourseUpdate}
      />, container);
    });
    expect(container.textContent).toBe("");
});

it("should set course name", () => {
    act(() => {
      render(
        <CourseComponent
        key={props.course.courseId}
        course={props.course}
        onCourseUpdate={props.onCourseUpdate}
      />, container);
    });
    
    let courseName: any = document.querySelector('[data-testid=courseName]');
    expect(courseName.textContent).toEqual("Mock course created");
  });

it("successfully dispatches events on heart click", () => {
    const onChange = jest.fn();
    act(() => {
        render( 
        <CourseComponent
            key={props.course.courseId}
            course={props.course}
            onCourseUpdate={onChange}/>, container);
    });
    
    const heart: any = document.querySelector("[data-testid=toggle]");
    
    act(() => {
        heart.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(onChange).toHaveBeenCalledTimes(1);
    
    act(() => {
        for (let i = 0; i < 5; i++) {
            heart.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        }
    });
    expect(onChange).toHaveBeenCalledTimes(6);
    
});