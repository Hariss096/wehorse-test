import * as React from "react";
import * as image from '../assets/logo192.png';

import { makeStyles } from '@material-ui/core/styles';
import { Grid } from "@material-ui/core";
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';


const useStyles = makeStyles((theme) => ({
  gridclass: {
    padding: "1%"
  },
  root: {
    display: "flex",
    width: "70%"
  },
  media: {
    width: "3em",
    height: "3em"
  },
  content: {
    padding: theme.spacing(1),
    backgroundColor: "floralWhite",
  },
  typo: {
    fontSize: "60%"
  },
  heart: {
    color: "green"
  }
}));


type Props = {
    course: Course
    onCourseUpdate: (course: Course) => void
}

export const CourseComponent: React.FC<Props> = ({ course, onCourseUpdate }) => {
  const classes = useStyles();
  const [addToWishList, setAddToWishList] = React.useState(course.wishListFlag);

  const toggleAddToWishList = () => {
    onCourseUpdate(course);
    setAddToWishList(!addToWishList);
  }

  return (
    <Grid container justify="center" className={classes.gridclass} spacing={2}>
        <Grid item xs={2} className={classes.content}>
          <CardMedia
            className={classes.media}
            image={image}
            title="Avatar">
          </CardMedia>
        </Grid>
        <Grid item xs={5} className={classes.content}>
          <span data-testid="courseName">
            <Typography variant="body2" className={classes.typo}>{course.courseName}</Typography>
          </span>
        </Grid>
        <Grid item xs={1} className={classes.content}>
          <span onClick={toggleAddToWishList} data-testid="toggle">
            {
              addToWishList ? <FavoriteIcon className={classes.heart}>{course.wishListFlag}</FavoriteIcon> :
              <FavoriteBorderIcon className={classes.heart}>{course.wishListFlag}</FavoriteBorderIcon>
              }
          </span>
        </Grid>
    </Grid>
  )
}