interface Course {
    courseId: number
    courseName: string
    wishListFlag: boolean
  }
  
type CourseState = {
  courseList: Course[]
}

type CourseAction = {
  type: string,
  course: Course,
  label: string
}
  
declare module "*.png" {
  const value: any;
  export = value;
}

declare module "*.json" {
  const value: any;
  export default value;
}

type DispatchType = (args: CourseAction) => CourseAction