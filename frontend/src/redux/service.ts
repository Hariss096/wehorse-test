import axios from 'axios';
const BASEURL = "http://localhost:5000/"


export const put = (apiEndpoint: string, payload: object) => {
    return axios.put(BASEURL+apiEndpoint, payload).then((response: any)=>{
        return response;
    }).catch((err: any)=>{
        console.log(err);
    })
}