import { combineReducers } from 'redux';
import * as actionTypes from "./actionTypes";
import courseList from "./courseList.json";


const initialState: CourseState = {
  courseList: courseList
}

const courseReducer = (
    state: CourseState = initialState, action: CourseAction): CourseState => {

    switch (action.type) {
      case actionTypes.UPDATE_COURSE:
        return Object.assign({}, state, {
          courseList: state.courseList.map(course => {
            if (course.courseId !== action.course.courseId) {
              return course;
            }
            return Object.assign({}, course, {
              wishListFlag: !course.wishListFlag
            })
          })
        })
      default: {
          return state;
        }
    }
  }


const rootReducer = combineReducers({
    CReducer   : courseReducer
});

export default rootReducer;