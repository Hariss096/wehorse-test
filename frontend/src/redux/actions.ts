import * as actionTypes from "./actionTypes";
import * as crudService from "./service";

export const editCourseInfo = (id: any, payload: any) => {
  return (dispatch: any) => {
      let apiEndpoint = 'courses/'+ id;
      crudService.put(apiEndpoint, payload)
      .then((response: any)=>{
      })
  }
}

export const updateWishList = (course: Course) => ({
  type: actionTypes.UPDATE_COURSE,
  course
});
