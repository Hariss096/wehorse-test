import React from 'react';
import { act } from "react-dom/test-utils";
import { render } from '@testing-library/react';
import { unmountComponentAtNode } from "react-dom";

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
 
import MainComponent from './MainComponent';

const mockStore = configureStore([]);

let store: any;
let container: any = null;
var props = {
        courseList : [{
        "courseId": 0,
        "courseName": "Mock course created",
        "wishListFlag": true
        }],
        editCourseInfo: (id: number, payload: any) => {},
        updateWishList: (course: object | any) => {}
      };

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
  store = mockStore({
    CReducer: {
      courseList: [{
        "courseId": 0,
        "courseName": "Mock course created",
        "wishListFlag": true
        }]
    }
  });
});

afterEach(() => {
  // cleanup on exit
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders successfully", () => {
  act(() => {
    render(
    <Provider store={store}>
      <MainComponent />
    </Provider>, container);
  });
  expect(container.textContent).toBe("");
});

it('should render with given state from Redux store', () => {
  act(() => {
    render(
    <Provider store={store}>
      <MainComponent />
    </Provider>, container)
  });
  expect(container).toMatchSnapshot();
});