Frontend for displaying courses list web application.

- Scripts to run in development mode:
    - install dependencies                      : npm install
    - for debugging                             : npm start
    - for testing                               : npm test

- Packages used:
    - React/React router                        : Javascript library for web app UIs
    - Redux/Redux-thunk                         : State management
    - Material UI                               : For styling and interactive icons
    - Jest                                      : For testing components
 
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)