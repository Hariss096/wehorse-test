# react-koa

A mono-repo for both front and backend of web application.


- Setup frontend:
    - [frontend/README.md](/frontend/README.md)

- Setup backend:
    - [backend/README.md](/backend/README.md)
